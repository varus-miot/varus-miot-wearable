# VARUS MIoT #



### What is VARUS MIoT? ###

**VARUS MIoT** (**V**arious **A**nalytical **R**esources **U**sed for **S**urveillance –  **M**edical **I**nternet **o**f **T**hings 
is a wrist-worn medical monitoring device that collects, processes, and transmits real-time data and / or video 
from patient diagnostic sensors (thermometers, accelerometers, heart rate & blood pressure sensors, kidney dialysis machines) 
and incubated biosamples to hosting devices (smart phones, tablets, PCs) via Bluetooth Low Energy (LE) wireless connection.